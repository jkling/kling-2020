const NOTION_API = "https://notion-api.splitbee.io/v1/page/";

export const getPage = async (pageId, context) => {
    const res = await fetchData(pageId, context);
    const postResponse = await res.json();

    const formattedPageId = pageId.replace(/-/g, "");
    return Object.entries(postResponse)
        .filter(([key, value]) => {
            if (
                !value.value ||
                !value.value.properties ||
                !value.value.properties.title ||
                !value.value.properties.title[0] ||
                !value.value.properties.title[0][0]
            ) {
                return false;
            }

            const formattedKey = key.replace(/-/g, "");
            const formattedParentId = value.value.parent_id.replace(/-/g, "");

            return (
                formattedKey == formattedPageId ||
                formattedPageId == formattedParentId
            );
        })
        .map(([_, value]) => value.value);
};

export const getEntries = async (pageId, context) => {
    const res = await fetchData(pageId, context);

    const postResponse = await res.json();

    return Object.entries(postResponse)
        .filter(([key, value]) => {
            const formattedKey = key.replace(/-/g, "");
            return formattedKey != pageId && value.value.type === "page";
        })
        .map(([key, value]) => {
            return {
                pageId: key,
                title: value.value.properties.title[0][0],
                timestamp: value.value.created_time,
            };
        });
};

const fetchData = async (pageId, context) => {
    const pageUrl = `${NOTION_API}${pageId}`;
    if (context && context.fetch) {
        return await context.fetch(pageUrl);
    } else {
        return await fetch(pageUrl);
    }
};
