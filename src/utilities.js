import fs from "fs";
import path from "path";

export const getFileData = (filesPath, fileName) => {
    return fs.readFileSync(path.resolve(filesPath, fileName), "utf-8");
};

export const getFiles = (filesPath, extension) => {
    const files = fs
        .readdirSync(filesPath)
        .filter((name) => (extension ? name.endsWith(extension) : true))
        .map((name) => {
            return { fileName: name, data: getFileData(filesPath, name) };
        });
    return files;
};

export const getJsonIn = (filesPath) => {
    const fileData = getFiles(filesPath, "json");
    return fileData.map((d) => JSON.parse(d.data));
};
