import { getJsonIn } from "../utilities";

export function get(req, res) {
    const data = getJsonIn("src/data/uses");

    res.writeHead(200, {
        "Content-Type": "application/json",
    });
    res.end(JSON.stringify(data));
}
