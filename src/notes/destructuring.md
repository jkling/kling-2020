---
title: Destructuring in JS
date: "2020-05-26"
author: "Jared"
labels: ["JavaScript"]
---

General destructuring example

```
let obj = {
    name: "Jared",
    city: "Chicago"
}

const { name, city } = obj;

console.log(name); // Jared
```

Assigning a new variable name

```
let obj = {
    name: "Jared",
    city: "Chicago"
}

const { name: firstName, city } = obj;

console.log(firstName); // Jared
```
