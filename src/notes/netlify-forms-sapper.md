---
title: Sapper / Netlify Forms
date: "2020-05-29"
author: "Jared"
labels: ["sapper", "netlify-forms", "netlify"]
---

I've been playing around a ton with Sapper to try to get Netlify Forms working properly.

After plenty of attempts, I finally have something working.

I ended up building a component to wrap all of my Netlify Forms so that I don't have to go through this again.

Here is what it looks like for now

```
<script>
  export let formName = "";
  export let action = "/thanks";
  export let honeypot = "";
</script>

<style>
  /* ... a bunch of my styles */
</style>

<form
  name={formName}
  method="POST"
  data-netlify="true"
  {action}
  netlify-honeypot={honeypot}
  content-type="application/x-www-form-urlencoded">
  <input type="hidden" name="form-name" value={formName} />
  <slot />
  <button type="submit">Send</button>
</form>
```

You can see that I am passing in the form name as a prop, I am using a honeypot as well and passing in the name of that input. The last prop is the `action` which is where the page will redirect to upon submit.

Hope this helps someone!
